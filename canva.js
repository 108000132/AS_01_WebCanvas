// When true, moving the mouse draws on the canvas
let isDrawing = false;
let isEra = false;
let isText = false;
let isCircle = false;
let isTriangle = false;
let isRectangle = false;
let x = 0;
let y = 0;
undo_l= new Array;
redo_l= new Array;

var canvas = document.getElementById('draw');
var ctx = canvas.getContext('2d');
var era = document.getElementById('eraser');
var pen = document.getElementById('pen1');
var text = document.getElementById('text');
var word = document.getElementById('wordSize');
var type = document.getElementById('wordFamily');
var brush = document.getElementById('range');
var texttp = document.getElementById('texttp');
var circle = document.getElementById('circle');
var triangle = document.getElementById('triangle');
var rectangle = document.getElementById('rectangle');
let upload = document.getElementById('upload');


word.value = 12;
var myWordSize = word.value;
var myWordFamily = type.value;
var linewidth = 25;
var texttxt = "";

brush.oninput = function(){
  linewidth = parseInt(this.value,10);
}

canvas.setAttribute("id","penIcon");


era.addEventListener('click',e =>{
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    //document.getElementById("draw").style.cursor = "cell";
    isEra = true;
    canvas.setAttribute("id","eraserIcon");
});

pen.addEventListener('click',e =>{
  isEra = false;
  canvas.setAttribute("id","penIcon");
});

text.addEventListener('click',e =>{
  canvas.setAttribute("id","textIcon");
  isText = true;
  isDrawing = false;
  isEra = false;
});

circle.addEventListener('click',e=>{
  canvas.setAttribute("id","circleIcon");
  isCircle = true;
  isDrawing = false;
  isEra = false;
});

triangle.addEventListener('click',e=>{
  canvas.setAttribute("id","triangleIcon");
  isTriangle = true;
  isDrawing = false;
  isEra = false;
});

rectangle.addEventListener('click',e=>{
  canvas.setAttribute("id","rectangleIcon");
  isRectangle = true;
  isDrawing = false;
  isEra = false;
});

upload.addEventListener("change",e =>{
   var URL = window.webkitURL || window.URL;
    var url = URL.createObjectURL(e.target.files[0]);
    var img = new Image();
    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0);
    }
    img.src = url;
});



word.addEventListener('change',e =>{
  myWordhSize = word.options[word.selectedIndex].value;
});

type.addEventListener('change',e =>{
  myWordFamily = type.options[type.selectedIndex].value;
})



// The x and y offset of the canvas from the edge of the page
const rect = canvas.getBoundingClientRect();

// Add the event listeners for mousedown, mousemove, and mouseup
canvas.addEventListener('mousedown', e => {
  x = e.clientX - rect.left;
  y = e.clientY - rect.top;
  isDrawing = true;
});


canvas.addEventListener('mousemove', e => {
  if (isDrawing === true) {
    drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
    x = e.clientX - rect.left;
    y = e.clientY - rect.top;
  }
});


window.addEventListener('mouseup', e => {
  if (isDrawing === true) {
    drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top);
    x = 0;
    y = 0;
    isDrawing = false;
  }
  if(isText === true){
    isDrawing = false;
    drawText(ctx,x,y,e.clientX-rect.left,e.clientY-rect.top);
    x=0;
    y=0;
  }
  if(isRectangle === true){
    isDrawing = false;
    drawRectangle(ctx,x,y,e.clientX-rect.left,e.clientY-rect.top);
    x=0;
    y=0;
  }
  if(isTriangle === true){
    isDrawing = false;
    drawTriangle(ctx,x,y,e.clientX-rect.left,e.clientY-rect.top);
    x=0;
    y=0;
  }
  if(isCircle === true){
    isDrawing = false;
    drawCircle(ctx,x,y,e.clientX-rect.left,e.clientY-rect.top);
    x=0;
    y=0;
  }
});


function drawLine(ctx, x1, y1, x2, y2) {
  ctx.beginPath();
  if(isEra === true)
    ctx.strokeStyle = 'white';
  else
    ctx.strokeStyle = document.getElementById('colors').value;
  ctx.lineWidth = linewidth;
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
  ctx.closePath();
}

function drawText(ctx,x1,y1,x2,y2){
  texttp.type = "type";
  texttxt = texttp.value;
  texttp.style.position = "absolute";
  texttp.style.top = y2 + "px";
  texttp.style.left = x2+ "px";
  texttp.addEventListener('keydown',e=>{
    if(event.key === "Enter"){
      ctx.fillStyle = document.getElementById('colors').value;
      ctx.font = myWordSize + "px" + " "+ myWordFamily;
      ctx.fillText(texttxt,x2,y2);
      texttp.type = "hidden";
    }
  });
}

function drawCircle(ctx,x1,y1,x2,y2){
  ctx.beginPath();
  ctx.lineCap = "butt";
  ctx.fillStyle = document.getElementById('colors').value;
  var rx = Math.abs(x2-x1)/2;
  var ry = Math.abs(y2-y1)/2;
  var dx = (x1-x2)/2;
  var dy = (y1-y2)/2;
  ctx.ellipse(x2+dx,y2+dy,rx,ry,0,0,2*Math.PI);
  ctx.fill();
}

function drawTriangle(ctx,x1,y1,x2,y2){
  ctx.beginPath();
  ctx.lineCap = "butt";
  ctx.fillStyle = document.getElementById('colors').value;
  var dx = (x2-x1);
  var dy = (y2-y1);
  var a1 = x2;
  var a2 = x2+dx/2;
  var a3 = x2+dx;
  var b1 = y2+dy;
  var b2 = y2;
  var b3 = y2+dy;
  ctx.moveTo(a1, b1);
  ctx.lineTo(a2, b2);
  ctx.lineTo(a3, b3);
  ctx.fill();
  ctx.closePath();
}

function drawRectangle(ctx,x1,y1,x2,y2){
  ctx.beginPath();
  ctx.lineCap = "butt";
  ctx.fillStyle = document.getElementById('colors').value;
  var dx = (x1-x2);
  var dy = (y1-y2);
  var a1 = x2;
  var a2 = x2+dx;
  var a3 = x2;
  var a4 = x2+dx;
  var b1 = y2;
  var b2 = y2;
  var b3 = y2+dy;
  var b4 = y2+dy;
  ctx.moveTo(a1, b1);
  ctx.lineTo(a2, b2);
  ctx.lineTo(a4, b4);
  ctx.lineTo(a3, b3);
  
  ctx.fill();
  ctx.closePath();
}

function Upload(e) {
  backgroundClear();
  canvasClear();
  
  var URL = window.webkitURL || window.URL;
  var url = URL.createObjectURL(e.target.files[0]);
  var img = new Image();
  img.onload = function() {
      canvas.width = img.width;
      canvas.height = img.height;

      background.width = img.width;
      background.height = img.height;
      background_ctx.drawImage(img, 0, 0);
  }
  img.src = url;
}

function download() {
  var a = document.createElement("a");
  document.body.appendChild(a);
  a.href = canvas.toDataURL();
  a.download = "zoouoo.png";
  a.click();
  document.body.removeChild(a);
}

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

var cPushArray = new Array();
var cStep = -1;
	

function undo(){
  ctx.globalCompositeOperation = 'source-over';
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  rss(canvas,ctx,undo_l,redo_l);
}

function redo(){
  ctx.globalCompositeOperation = 'source-over';
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  rss(canvas,ctx,redo_l,undo_l);
}

function ss(canvas,list,kredo){
  kredo = kredo || false;
      if(!kredo) {
        redo_list = [];
      }
      (list || undo_list).push(canvas.toDataURL());  
}

function rss(canvas,ctx,pop,push){
  if(pop.length){
    ss(canvas,push,true);
    var rss = pop.pop();
    var img = document.createElement('imgur');
    imgur.src = rss;
    img.onload = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);  
      }
  }
}



