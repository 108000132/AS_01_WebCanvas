# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    ![](https://i.imgur.com/fcN9jVr.png)
    上面是繪圖區,下面是功能區
    reset:
    在繪圖區裡,有設置reset,可以按下去以後將canvas的內容清除。
    Color:
    這是調色盤,筆與三角形,圓形,正方形的顏色都跟該調色盤所點選的顏色相同。
    橡皮擦:
    用來擦掉畫錯的圖案。
    筆刷粗細條:
    可以用來調整筆刷粗細。
    筆:
    繪圖用
    TEXT:
    可以用來在canvas打字。
    圓形:
    可以繪製圓型。
    三角形:
    可以繪製三角形。
    正方形:
    可以繪製正方形。
    下載:
    可以用來下載canvas所畫的圖片。
    上傳:
    可以上傳圖片。
    un/redo:
    可以調成上一步或下一步。

    Describe how to use your web and maybe insert images to help you explain.

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"
    https://108000132.gitlab.io/AS_01_WebCanvas

### Others (Optional)
    
    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
